**Hello! I see you've found my GitLab profile.** 🙂

I recently decided to move away from GitHub in an effort to support open source software and not contribute to the monopoly of large tech companies.

Currently I work a lot with [Kotlin](https://kotlinlang.org/) and [Android](https://www.android.com/) (and I am loving it!) but I also have worked with Java, JavaScript & TypeScript, Python, and Swift in a variety of different ecosystems.

**Some of my projects:**

- [UpcomingMCU API](https://gitlab.com/upcomingmcu/api) - The ultimate source for staying up-to-date with the MCU.
- [UpcomingMCU Android App](https://gitlab.com/upcomingmcu/android-app) - The official UpcomingMCU app for Android.
- [Anibl](https://gitlab.com/aniable/anibl) - A free and open-source URL shortener.
- More coming soon!

**Other places you can find me:**

- [My website](https://seano.dev/)
- [GitHub](https://github.com/seaneoo) (Inactive)
- [LinkedIn](https://www.linkedin.com/in/seaneoo/)
- [Liberapay](https://en.liberapay.com/seaneoo/)

If you have any questions or just want to chat, please email me: **hey (at) seano.dev**.
